__author__ = 'john'
import hashlib
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth import get_user_model


def check_joomla_password(row_password, passowrd_hash):
    try:
        hash, solt = passowrd_hash.split(':')
    except ValueError:
        return False

    m = hashlib.md5()
    m.update(row_password+solt)

    if hash == m.hexdigest():
        return True

    return False


class JoomlaBackend(ModelBackend):
    def authenticate(self, username=None, password=None, **kwargs):
        UserModel = get_user_model()
        if username is None:
            username = kwargs.get(UserModel.USERNAME_FIELD)
        try:
            user = UserModel._default_manager.get_by_natural_key(username)

            if check_joomla_password(password, user.password):
                return user
        except UserModel.DoesNotExist:
            return None
